#include <Arduino.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <WiFi.h>

#define VERSION "0.2.0"
#define LED 5
#define WIFI_RESET 0
#define SEM_PERIOD 500

// Mqtt Server
#define MQTT_SERVER "mqtt.adcm.co.th"
#define MQTT_USER "adcm"
#define MQTT_PASSWORD "p@ssw0rd"

String ssid = "adcm";
String password = "adcm1234";

// Mqtt Config
const uint64_t chipid = ESP.getEfuseMac();
const String device_id = String((uint16_t)(chipid >> 32), HEX) + String((uint32_t)chipid, HEX);

const String statusTopic = "device/" + String(device_id) + "/status";
const String dataTopic = "device/" + String(device_id) + "/data";
const String pinOnTopic = "pin/" + String(device_id) + "/on";
const String pinOnResultTopic = "pin/" + String(device_id) + "/on/result";
const String pinOffTopic = "pin/" + String(device_id) + "/off";
const String pinOffResultTopic = "pin/" + String(device_id) + "/off/result";
const String pinReadTopic = "pin/" + String(device_id) + "/read";
const String pinReadResultTopic = "pin/" + String(device_id) + "/read/result";
const String volumeSolutionTopic = "solution/" + String(device_id);
const String volumeSolutionResultTopic = "solution/" + String(device_id) + "/result";

// MQTT Sub Pub Client
WiFiClient client;
PubSubClient mqttClient(client);

// Pin Control Devices
int pins[] = {25, 26, 27, 32};
int solution[] = {}; //{ec,ph}

// Timer
TimerHandle_t tmr;
boolean flash = false;
SemaphoreHandle_t semI2C;

// Task (Multitasking)
void vTaskMqtt(void *pvParameters);
void vTaskSensor(void *pvParameters);

void ping(TimerHandle_t xTimer)
{
  if (!flash)
  {
    digitalWrite(LED, !digitalRead(LED));
  }
  else
  {
    digitalWrite(LED, HIGH);
    vTaskDelay(100 / portTICK_PERIOD_MS);
    digitalWrite(LED, LOW);
  }
}

void subscribe()
{
  mqttClient.subscribe(pinOnTopic.c_str());
  mqttClient.subscribe(pinOffTopic.c_str());
  mqttClient.subscribe(pinReadTopic.c_str());
  mqttClient.subscribe(volumeSolutionTopic.c_str());
}

void callback(char *topic, byte *payload, unsigned int length)
{
  String _topic = String(topic);
  String _payload = String((char *)payload).substring(0, length);
  Serial.println(_topic);
  DynamicJsonDocument doc(1024);
  doc.clear();
  if (_topic == pinReadTopic)
  {
    if (xSemaphoreTake(semI2C, SEM_PERIOD) == pdTRUE)
    {
      doc["device_id"] = device_id;
      JsonArray arry = doc.createNestedArray("pin");
      for (int n = 0; n < 4; n++)
      {
        JsonObject obj = arry.createNestedObject();
        obj["index"] = n;
        obj["state"] = digitalRead(pins[n]);
      }
      xSemaphoreGive(semI2C);
    }
    String jsonDoc = String();
    serializeJson(doc, jsonDoc);
    Serial.println(jsonDoc);
    Serial.println(pinReadResultTopic);
    mqttClient.publish(pinReadResultTopic.c_str(), jsonDoc.c_str());
  }

  if (_topic == pinOnTopic)
  {
    if (xSemaphoreTake(semI2C, SEM_PERIOD) == pdTRUE)
    {
      deserializeJson(doc, payload);
      int index = doc["index"].as<int32_t>();
      digitalWrite(pins[index], HIGH);
      doc.clear();
      doc["device_id"] = device_id;
      doc["index"] = index;
      doc["state"] = digitalRead(pins[index]);
      xSemaphoreGive(semI2C);
    }
    String jsonDoc = String();
    serializeJson(doc, jsonDoc);
    Serial.println(jsonDoc);
    mqttClient.publish(pinOnResultTopic.c_str(), jsonDoc.c_str());
  }

  if (_topic == pinOffTopic)
  {
    if (xSemaphoreTake(semI2C, SEM_PERIOD) == pdTRUE)
    {
      deserializeJson(doc, payload);
      int index = doc["index"].as<int32_t>();
      digitalWrite(pins[index], LOW);
      doc.clear();
      doc["device_id"] = device_id;
      doc["index"] = index;
      doc["state"] = digitalRead(pins[index]);
      xSemaphoreGive(semI2C);
    }
    String jsonDoc = String();
    serializeJson(doc, jsonDoc);
    Serial.println(jsonDoc);
    mqttClient.publish(pinOffResultTopic.c_str(), jsonDoc.c_str());
  }
  if (_topic == volumeSolutionTopic)
  {
    if (xSemaphoreTake(semI2C, SEM_PERIOD) == pdTRUE)
    {
      deserializeJson(doc, payload);
      solution[0] = doc["ec"];
      solution[1] = doc["ph"];
      doc.clear();
      doc["device_id"] = device_id;
      doc["ec"] = solution[0];
      doc["ph"] = solution[1];
      xSemaphoreGive(semI2C);
    }
    String jsonDoc = String();
    serializeJson(doc, jsonDoc);
    Serial.println(jsonDoc);
    mqttClient.publish(volumeSolutionResultTopic.c_str(), jsonDoc.c_str());
  }
}

void register_in()
{
  String registerTopic = "device/" + device_id + "/register";
  DynamicJsonDocument doc(512);
  JsonObject addr = doc.createNestedObject("addr");
  addr["ip"] = WiFi.localIP().toString();
  addr["subnet"] = WiFi.subnetMask().toString();
  addr["gateway"] = WiFi.gatewayIP().toString();
  addr["rssi"] = WiFi.RSSI();
  doc["device_id"] = device_id;
  doc["version"] = VERSION;
  String jsonDoc = String();
  serializeJson(doc, jsonDoc);
  Serial.println(jsonDoc);
  mqttClient.publish(registerTopic.c_str(), jsonDoc.c_str());
}

void mqtt_init()
{
  mqttClient.setServer(MQTT_SERVER, 1883);
  mqttClient.setCallback(callback);
  mqttClient.setKeepAlive(10);
  String payload = "{\"device_id\": \"" + device_id + "\"}";
  if (mqttClient.connect(device_id.c_str(), MQTT_USER, MQTT_PASSWORD, statusTopic.c_str(), 0, false, payload.c_str()))
  {
    Serial.println("MQTT client connect success");
    subscribe();
    register_in();
    xTaskCreatePinnedToCore(vTaskMqtt, "vTaskMqtt", 1024 * 4, NULL, tskIDLE_PRIORITY - 5, NULL, 1);
    xTaskCreatePinnedToCore(vTaskSensor, "vTaskSensor", 1024 * 8, NULL, tskIDLE_PRIORITY - 5, NULL, 1);
  }
  else
  {
    Serial.println("MQTT client connect failed");
  }
}

void vTaskMqtt(void *pvParameters)
{
  while (true)
  {
    if (mqttClient.connected())
    {
      mqttClient.loop();
    }
    else
    {
      vTaskDelay(2000 / portTICK_PERIOD_MS);
      if (mqttClient.connect(device_id.c_str(), MQTT_USER, MQTT_PASSWORD, statusTopic.c_str(), 0, false, "{}"))
      {
        subscribe();
        register_in();
        Serial.println("MQTT client reconnect");
      }
      else
      {
        Serial.println("MQTT client reconnect failed");
      }
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
  }
  vTaskDelete(NULL);
}

void vTaskSensor(void *pvParameters)
{
  vTaskDelay(1000 / portTICK_PERIOD_MS);
  while (true)
  {
    // random value for test
    float humi = (float)(random(100, 999) / 10.0);
    float temp = (float)(random(100, 999) / 10.0);
    uint32_t lux = random(2000, 65535);
    uint32_t soil = random(20, 100);

    DynamicJsonDocument doc(512);
    doc["device_id"] = device_id;
    doc["temperature"] = double((int(temp * 10)) / 10.0);
    doc["humidity"] = double((int(humi * 10)) / 10.0);
    doc["light"] = lux;
    doc["soil"] = soil;
    String jsonDoc = String();
    serializeJson(doc, jsonDoc);
    Serial.println(jsonDoc);
    if (mqttClient.connected())
    {
      mqttClient.publish(dataTopic.c_str(), jsonDoc.c_str());
    }
    vTaskDelay(15000 / portTICK_PERIOD_MS);
  }
  vTaskDelete(NULL);
}

void WiFiStationConnected(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("Connected to AP!");
  Serial.print("SSID Length: ");
  Serial.println(info.wifi_sta_connected.ssid_len);

  Serial.print("SSID: ");
  for (int i = 0; i < info.wifi_sta_connected.ssid_len; i++)
  {
    Serial.print((char)info.wifi_sta_connected.ssid[i]);
  }

  Serial.print("\nBSSID: ");
  for (int i = 0; i < 6; i++)
  {
    Serial.printf("%02X", info.wifi_sta_connected.bssid[i]);
    if (i < 5)
    {
      Serial.print(":");
    }
  }
  Serial.print("\nChannel: ");
  Serial.println(info.wifi_sta_connected.channel);

  Serial.print("Auth mode: ");
  Serial.println(info.wifi_sta_connected.authmode);
}

void WiFiStationGotIp(WiFiEvent_t event, WiFiEventInfo_t info)
{
  Serial.println("WiFi got ip address");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  flash = true;
  xTimerChangePeriod(tmr, 2000, 0);
  mqtt_init();
}

void setup()
{
  Serial.begin(115200);
  pinMode(LED, OUTPUT);
  pinMode(WIFI_RESET, INPUT_PULLUP);
  delay(100);

  // Timer
  semI2C = xSemaphoreCreateMutex();
  tmr = xTimerCreate("MyTimer", 100, pdTRUE, NULL, &ping);
  xTimerStart(tmr, 0);

  // Set Pins : OUTPUT
  for (int i = 0; i < 4; i++)
  {
    pinMode(pins[i], OUTPUT);
  }

  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.mode(WIFI_STA);

  WiFi.onEvent(WiFiStationConnected, ARDUINO_EVENT_WIFI_STA_CONNECTED);
  WiFi.onEvent(WiFiStationGotIp, ARDUINO_EVENT_WIFI_STA_GOT_IP);

  WiFi.begin(ssid.c_str(), password.c_str());
  Serial.print("DEVICE ID: ");
  Serial.println(device_id);
}

void loop() {}